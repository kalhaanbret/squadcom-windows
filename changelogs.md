***SquadCom 0.0.1***
- Création d'avatars
- Site Web (pc/consoles)
- Application (pc)
-------------------------------
***SquadCom 0.0.2***
- Exe : Amélioration des appels Apis / optimisations lecture logs / lecture status asap
- Bugs Fixs application/site web
-------------------------------
***SquadCom 0.0.2a***
- Site web : (pb consoles) Contournement bug logs consoles de frontier : miss commander name...
--------------------------------
***SquadCom 0.0.3***
- Changements interface
- Rajout des valeurs de scan / map système actuel
- Rajout des matériaux et rings
- Exe : Amélioration de système de switch du lecteur de logs (local / capi frontier)
---------------------------------
***SquadCom 0.0.4***

**Application et site :**
- Thème Elite Dangerous aux panneaux

**Application seulement :**
- Liaison entre Elite et Squadcom : envois d'inputs claviers à Elite
- Intégration des panneaux dans le cockpit
- Lien : http://thelosp.fr/temp/SquadComSetup0.0.4.rar
- Site : http://squadcom.edtfm.com
----------------------------------
***SquadCom 0.0.5***
- Hud Helmet avec informations de status (Application pc uniquement)
- Test d'un "widget" Helmet : Landing Process (Application pc uniquement)
- Navigation Joystick / Clavier dans les pannaux (Application et Site Web)
- Ajout des informations edsm détaillée dans le panneau Navigation (Application et Site Web)
- Ajout des informations de vaisseau, de modules et de blueptints d'ingénieurs (Application et Site Web)
