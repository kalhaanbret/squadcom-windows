***Squad'com Alpha edition***
description is coming soon 

***Conditions de fonctionnement de l'alpha***

**Mode de réglage d'élite** : Plein Ecran / Plein Ecran sans bordure (borderless)

**IMPORTANT : Ne pas utilisé en fenêtré pour le moment**

Cette version ne prend pas en charge la  persistance des données prévue en 0.0.6, cela signifie qu'aucune information n'est sauvé en local ou dans le cloud.

La configuration des touches et joystick n'est pas active pour le moment : 

**Navigation entre les panneaux :**

|  Fonction | Touche clavier  | Touche X-56 |
|---|---|---|
|Panneau Gauche   | &  | Joystick/stick rond Gris/Gauche   |
|Panneau Bas | " | Joystick/stick rond Gris/Bas |
|Panneau Droite | ' | Joystick/stick rond Gris/Droite |
|UI Gauche| q | Joystick/stick bas-droite/Gauche |
|UI Droite| d| Joystick/stick bas-droite/Droite|
|UI Bas| s| Joystick/stick bas-droite/Bas|
|UI Haut| z| Joystick/stick bas-droite/Haut|
|UI Panneau suivant| e | Hotas/stick gris superieur/gauche|
|UI Panneau précédent| a | Hotas/stick gris superieur/droite|



